﻿namespace Knapsack

type public MyFullEnumerationSolver() =
  interface ISolver with
    member _this.GetName() = "Полный перебор"
    member _this.Solve(M, m, c) = 
      let weights = List.ofArray m // Теперь в weights обычный F#-список масс ...
      let costs = List.ofArray c   // ... а в costs  - обычный F#-список стоимостей

      let rec solveRec M weights (costs:list<int>) content myCost myWeight = 
       if (weights = []) then 
        (myCost, List.rev (content))
       else
        if (myWeight + weights.Head <= M) then
         let put = solveRec M weights.Tail costs.Tail (true::content) (myCost + costs.Head) (myWeight + weights.Head)
         let notput = solveRec M weights.Tail costs.Tail (false::content) myCost myWeight
         if (fst(put) > fst(notput)) then
          put
         else 
          notput
        else
         solveRec M weights.Tail costs.Tail (false::content) myCost myWeight

      let solve M weights costs =
         snd(solveRec M weights costs [] 0 0)

      List.toArray (solve M weights costs) // в конце с помощью toArray F#-список превращается в массив