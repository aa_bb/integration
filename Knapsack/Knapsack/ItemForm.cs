﻿using System.Windows.Forms;

namespace Knapsack
{
    public partial class ItemForm : Form
    {
        KnapsackForm parent;
        int id;
        string article;
        int weight;
        int cost;
        bool isAddForm;

        public ItemForm(KnapsackForm parent)
        {
            this.parent = parent;
            isAddForm = true;
            InitializeComponent();
        }

        public ItemForm(KnapsackForm parent, int id, string article, int weight, int cost)
        {
            this.parent = parent;
            this.id = id;
            isAddForm = false;
            InitializeComponent();
            this.article = article;
            this.weight = weight;
            this.cost = cost;
            articleTB.Text = article;
            weightTB.Text = weight.ToString();
            costTB.Text = cost.ToString();
        }

        private void articleTB_TextChanged(object sender, System.EventArgs e)
        {
            article = articleTB.Text;
        }

        private void weightTB_TextChanged(object sender, System.EventArgs e)
        {
            int temp;
            if (int.TryParse(weightTB.Text, out temp))
                weight = temp;
        }

        private void costTB_TextChanged(object sender, System.EventArgs e)
        {
            int temp;
            if (int.TryParse(costTB.Text, out temp))
                cost = temp;
        }

        private void finishButton_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(article) || weight == 0 || cost == 0)
            {
                MessageBox.Show("Некорректный ввод");
                return;
            }

            if (isAddForm)
                parent.AddItem(article, weight, cost);
            else
                parent.UpdateItem(id, article, weight, cost);
            Close();
        }
    }
}
