﻿namespace Knapsack
{
    partial class ItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.articleTB = new System.Windows.Forms.TextBox();
            this.weightTB = new System.Windows.Forms.TextBox();
            this.costTB = new System.Windows.Forms.TextBox();
            this.finishButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Предмет";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Вес";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Цена";
            // 
            // articleTB
            // 
            this.articleTB.Location = new System.Drawing.Point(108, 40);
            this.articleTB.Name = "articleTB";
            this.articleTB.Size = new System.Drawing.Size(210, 26);
            this.articleTB.TabIndex = 0;
            this.articleTB.TextChanged += new System.EventHandler(this.articleTB_TextChanged);
            // 
            // weightTB
            // 
            this.weightTB.Location = new System.Drawing.Point(108, 92);
            this.weightTB.Name = "weightTB";
            this.weightTB.Size = new System.Drawing.Size(210, 26);
            this.weightTB.TabIndex = 1;
            this.weightTB.TextChanged += new System.EventHandler(this.weightTB_TextChanged);
            // 
            // costTB
            // 
            this.costTB.Location = new System.Drawing.Point(108, 144);
            this.costTB.Name = "costTB";
            this.costTB.Size = new System.Drawing.Size(210, 26);
            this.costTB.TabIndex = 2;
            this.costTB.TextChanged += new System.EventHandler(this.costTB_TextChanged);
            // 
            // finishButton
            // 
            this.finishButton.Location = new System.Drawing.Point(97, 193);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(156, 32);
            this.finishButton.TabIndex = 3;
            this.finishButton.Text = "OK";
            this.finishButton.UseVisualStyleBackColor = true;
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // ItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 242);
            this.Controls.Add(this.finishButton);
            this.Controls.Add(this.costTB);
            this.Controls.Add(this.weightTB);
            this.Controls.Add(this.articleTB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ItemForm";
            this.Text = "Предмет";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox articleTB;
        private System.Windows.Forms.TextBox weightTB;
        private System.Windows.Forms.TextBox costTB;
        private System.Windows.Forms.Button finishButton;
    }
}