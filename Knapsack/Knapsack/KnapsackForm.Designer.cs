﻿namespace Knapsack
{
    partial class KnapsackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.алгоритмToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Article = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Weight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Cost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.addButton = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.maxWeightTB = new System.Windows.Forms.TextBox();
            this.solveTaskButton = new System.Windows.Forms.Button();
            this.выбратьDllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.алгоритмToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(616, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(65, 29);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(252, 30);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(252, 30);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // алгоритмToolStripMenuItem
            // 
            this.алгоритмToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выбратьDllToolStripMenuItem});
            this.алгоритмToolStripMenuItem.Name = "алгоритмToolStripMenuItem";
            this.алгоритмToolStripMenuItem.Size = new System.Drawing.Size(104, 29);
            this.алгоритмToolStripMenuItem.Text = "Алгоритм";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Article,
            this.Weight,
            this.Cost});
            this.listView1.Location = new System.Drawing.Point(20, 50);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(338, 298);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Article
            // 
            this.Article.Text = "Предмет";
            this.Article.Width = 82;
            // 
            // Weight
            // 
            this.Weight.Text = "Вес";
            this.Weight.Width = 40;
            // 
            // Cost
            // 
            this.Cost.Text = "Цена";
            this.Cost.Width = 212;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(396, 51);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(184, 31);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Добавить предмет";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // delButton
            // 
            this.delButton.Location = new System.Drawing.Point(396, 104);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(184, 31);
            this.delButton.TabIndex = 3;
            this.delButton.Text = "Удалить предмет";
            this.delButton.UseVisualStyleBackColor = true;
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(396, 159);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(184, 31);
            this.updateButton.TabIndex = 4;
            this.updateButton.Text = "Изменить предмет";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 375);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Максимальный вес рюкзака:";
            // 
            // maxWeightTB
            // 
            this.maxWeightTB.Location = new System.Drawing.Point(261, 372);
            this.maxWeightTB.Name = "maxWeightTB";
            this.maxWeightTB.Size = new System.Drawing.Size(319, 26);
            this.maxWeightTB.TabIndex = 6;
            this.maxWeightTB.TextChanged += new System.EventHandler(this.maxWeightTB_TextChanged);
            // 
            // solveTaskButton
            // 
            this.solveTaskButton.Location = new System.Drawing.Point(396, 287);
            this.solveTaskButton.Name = "solveTaskButton";
            this.solveTaskButton.Size = new System.Drawing.Size(184, 61);
            this.solveTaskButton.TabIndex = 7;
            this.solveTaskButton.Text = "Найти оптимальное решение";
            this.solveTaskButton.UseVisualStyleBackColor = true;
            this.solveTaskButton.Click += new System.EventHandler(this.solveTaskButton_Click);
            // 
            // выбратьDllToolStripMenuItem
            // 
            this.выбратьDllToolStripMenuItem.Name = "выбратьDllToolStripMenuItem";
            this.выбратьDllToolStripMenuItem.Size = new System.Drawing.Size(252, 30);
            this.выбратьDllToolStripMenuItem.Text = "Добавить dll";
            this.выбратьDllToolStripMenuItem.Click += new System.EventHandler(this.выбратьDllToolStripMenuItem_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "(Пусто)"});
            this.comboBox1.Location = new System.Drawing.Point(396, 205);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(183, 28);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // KnapsackForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 429);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.solveTaskButton);
            this.Controls.Add(this.maxWeightTB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "KnapsackForm";
            this.Text = "Задача о рюкзаке";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem алгоритмToolStripMenuItem;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Article;
        private System.Windows.Forms.ColumnHeader Weight;
        private System.Windows.Forms.ColumnHeader Cost;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button delButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox maxWeightTB;
        private System.Windows.Forms.Button solveTaskButton;
        private System.Windows.Forms.ToolStripMenuItem выбратьDllToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

