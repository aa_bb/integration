﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Knapsack
{
    public partial class KnapsackForm : Form
    {
        string filename = "";
        int maxWeight;
        List<int> weights;
        List<int> costs;
        ISolver solver;
        List<ISolver> methodCollection = new List<ISolver>() { null };

        public KnapsackForm()
        {
            InitializeComponent();
            weights = new List<int>();
            costs = new List<int>();
            comboBox1.SelectedIndex = 0;
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            if (!dlg.SafeFileName.EndsWith(".txt"))
            {
                MessageBox.Show("Неверный формат файла, выберите файл с расширением .txt");
                return;
            }
            filename = dlg.FileName;
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    ClearAllData();
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        try
                        {
                            var strs = line.Split(' ');
                            AddItem(strs[0], int.Parse(strs[1]), int.Parse(strs[2])); 
                        }
                        catch (Exception ex)
                        {
                            ClearAllData();
                            MessageBox.Show("Файл содержит некорректные данные, проверьте число и тип полей");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("Возникли проблемы с открытием файла, выберите другой файл");
                return;
            }
        }

        void ClearAllData()
        {
            listView1.Items.Clear();
            weights.Clear();
            costs.Clear();
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (filename == "")
            {
                var dlg = new OpenFileDialog();
                if (dlg.ShowDialog() != DialogResult.OK)
                    return;
                if (!dlg.SafeFileName.EndsWith(".txt"))
                {
                    MessageBox.Show("Неверный формат файла, выберите файл с расширением .txt");
                    return;
                }
                filename = dlg.FileName;
            }
            try
            {
                using (StreamWriter sw = new StreamWriter(filename, false))
                {
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            sw.Write(listView1.Items[i].SubItems[j].Text);
                            if (j < 2)
                                sw.Write(" ");
                        }
                        sw.WriteLine();
                    }
                }
                MessageBox.Show("Данные успешно сохранены");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("Возникли проблемы с открытием файла, выберите другой файл");
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            DropColorSelection();
            var f = new ItemForm(this);
            f.Show();
        }

        public void AddItem(string article, int weight, int cost)
        {
            var strs = new string[] { article, weight.ToString(), cost.ToString() };
            listView1.Items.Add(new ListViewItem(strs));
            weights.Add(weight);
            costs.Add(cost);
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                return;
            DropColorSelection();
            for (int i = listView1.Items.Count - 1; i >= 0; i--)
            {
                if (listView1.Items[i].Selected)
                {
                    listView1.Items[i].Remove();
                    weights.RemoveAt(i);
                    costs.RemoveAt(i);
                }
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                return;
            DropColorSelection();
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                if(listView1.Items[i].Selected)
                {
                    string article = listView1.Items[i].SubItems[0].Text;
                    int weight = int.Parse(listView1.Items[i].SubItems[1].Text);
                    int cost = int.Parse(listView1.Items[i].SubItems[2].Text);
                    var f = new ItemForm(this, i, article, weight, cost);
                    f.Show();
                }
            }
        }

        public void UpdateItem(int id, string article, int weight, int cost)
        {
            listView1.Items[id].SubItems[0].Text = article;
            listView1.Items[id].SubItems[1].Text = weight.ToString();
            listView1.Items[id].SubItems[2].Text = cost.ToString();
            weights[id] = weight;
            costs[id] = cost;
        }

        private void maxWeightTB_TextChanged(object sender, EventArgs e)
        {
            DropColorSelection();
            int temp;
            if (int.TryParse(maxWeightTB.Text, out temp))
                maxWeight = temp;
            if (maxWeightTB.Text == "")
                maxWeight = 0;
        }

        private void solveTaskButton_Click(object sender, EventArgs e)
        {
            if (maxWeight == 0)
            {
                MessageBox.Show("Поле 'Максимальный вес рюкзака' не заполнено или заполнено некорректно");
                return;
            }
            if (weights.Count == 0)
            {
                MessageBox.Show("Список предметов пуст, пополните его или загрузите данные из файла");
                return;
            }
            if (solver == null)
            {
                MessageBox.Show("Метод решения не выбран");
                return;
            }
            var result = solver.Solve(maxWeight, weights.ToArray(), costs.ToArray());
            ParseResultToMbox(result);
            ParseResultToListviewSelection(result);
        }

        void ParseResultToMbox(bool[] result)
        {
            var str = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
                if (result[i])
                    str.Append(i + 1 + " ");
            string res = str.Length == 0 ? "Ничего" : str.ToString();
            MessageBox.Show("Оптимальный выбор предметов:\n" + res);
        }

        void ParseResultToListviewSelection(bool[] result)
        {
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i])
                    listView1.Items[i].BackColor = Color.LightSeaGreen;
                else
                    listView1.Items[i].BackColor = Color.White;
            }
        }

        void DropColorSelection()
        {
            for (int i = 0; i < listView1.Items.Count; i++)
            {
               listView1.Items[i].BackColor = Color.White;
            }
        }

        private void выбратьDllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            var allTypes = Assembly.LoadFile(dlg.FileName).GetTypes();
            foreach (var t in allTypes)
            {
                var i = t.GetInterface("ISolver");
                if (i == null)
                    continue;
                foreach (var c in t.GetConstructors())
                    if (c.GetParameters().Length == 0)
                    {
                        solver = c.Invoke(new object[0]) as ISolver;
                        methodCollection.Add(solver);
                        comboBox1.Items.Add(solver.GetName());
                        comboBox1.SelectedItem = solver.GetName();
                        return;
                    }
            }
            MessageBox.Show("Dll не содержит функций, реализующих алгоритм");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropColorSelection();
            solver = methodCollection[comboBox1.SelectedIndex];
        }
    }
}
